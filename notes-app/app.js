// const fs = require('fs');
// //fs.writeFileSync('notes.txt', 'My name is aditya.');
// fs.appendFileSync('notes-one.txt', 'I am learner 2.');

// const add = require('./utils.js');

// const sum = add(4, -1);

// // const name = 'Aditya';
// console.log(sum);

// const fileContent = require('./notes.js');

// const fileContentData = fileContent();

// console.log(fileContentData);

// const validator =  require('validator');

// // console.log(validator.isEmail('mobeserv.com'));

// console.log(validator.isURL('https://mobeserv.com'));

// const chalk = require('chalk');
 
// console.log(chalk.blue('Hello world!'));

const chalk = require('chalk');
const log = console.log;
 
// Combine styled and normal strings
log(chalk.blue('Hello') + ' World' + chalk.red('!'));
 
// Compose multiple styles using the chainable API
log(chalk.blue.bgRed.bold('Hello world!'));
 
// Pass in multiple arguments
log(chalk.blue('Hello', 'World!', 'Foo', 'bar', 'biz', 'baz'));
 
// Nest styles
log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));
 
// Nest styles of the same type even (color, underline, background)
log(chalk.green(
    'I am a green line ' +
    chalk.blue.underline.bold('with a blue substring') +
    ' that becomes green again!'
));
 
// ES2015 template literal
// log(`
// CPU: ${chalk.red('90%')}
// RAM: ${chalk.green('40%')}
// DISK: ${chalk.yellow('70%')}
// `);
 
// // ES2015 tagged template literal
// log(chalk`
// CPU: {red ${cpu.totalPercent}%}
// RAM: {green ${ram.used / ram.total * 100}%}
// DISK: {rgb(255,131,0) ${disk.used / disk.total * 100}%}
// `);
 
// Use RGB colors in terminal emulators that support it.
log(chalk.keyword('orange')('Yay for orange colored text!'));
log(chalk.rgb(123, 45, 67).underline('Underlined reddish color'));
log(chalk.hex('#DEADED').bold('Bold gray!'));